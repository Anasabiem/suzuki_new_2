 <?php $this->load->view("user/side/head"); ?>
 <?php $this->load->view("user/side/navbar"); ?>
 <div class="c-layout-page">
 	<!-- BEGIN: LAYOUT/BREADCRUMBS/BREADCRUMBS-1 -->

 	<div class="c-layout-breadcrumbs-1 c-fonts-uppercase c-fonts-bold">
 		<div class="container">
 			<div class="c-page-title c-pull-left">
 				<h3 class="c-font-uppercase c-font-sbold">Gallery</h3>
 			</div>
 			<ul class="c-page-breadcrumbs c-theme-nav c-pull-right c-fonts-regular">
 				<li><a href="<?php echo base_url('admin/Home') ?>">Home</a></li>
 				<li>/</li>
 				<li><a href="#">Gallery</a></li>

 			</ul>
 		</div>
 	</div><!-- END: LAYOUT/BREADCRUMBS/BREADCRUMBS-1 -->
 	<!-- BEGIN: PAGE CONTENT -->
 	<div class="c-content-box c-size-md">
 		<div class="container">
 			<div class="cbp-panel">

 				<div id="grid-container" class="cbp cbp-l-grid-masonry-projects">
 					<?php foreach ($gambar->result() as $key): ?>
 						<div class="cbp-item graphic">
 						<div class="cbp-caption">
 							<div class="cbp-caption-defaultWrap">
 								<img style="height: 200px;" src="<?php echo  base_url()?><?php echo $key->foto; ?>" alt="">
 							</div>
 							<div class="cbp-caption-activeWrap">
 								<div class="c-masonry-border"></div>
 								<div class="cbp-l-caption-alignCenter">
 									<div class="cbp-l-caption-body">
 										<a href="<?php echo  base_url()?><?php echo $key->foto; ?>" class="cbp-lightbox cbp-l-caption-buttonRight btn c-btn-square c-btn-border-1x c-btn-white c-btn-bold c-btn-uppercase" data-title="">Zoom</a>
 									</div>
 								</div>
 							</div>
 						</div>
 						<a href="ajax/projects/project1.html" class="cbp-singlePage cbp-l-grid-masonry-projects-title"><?php echo $key->nama ?></a>
 						<!-- <div class="cbp-l-grid-masonry-projects-desc"></div> -->
 					</div>
 					<?php endforeach ?>
 				</div>

 				<!-- <div id="loadMore-container" class="cbp-l-loadMore-button c-margin-t-60">
 					<a href="ajax/masonry-gallery/load-more.html" class="cbp-l-loadMore-link btn c-btn-square c-btn-border-2x c-btn-dark c-btn-bold c-btn-uppercase">
 						<span class="cbp-l-loadMore-defaultText">LOAD MORE</span>
 						<span class="cbp-l-loadMore-loadingText">LOADING...</span>
 						<span class="cbp-l-loadMore-noMoreLoading">NO MORE WORKS</span>
 					</a>
 				</div> -->
 			</div>
 		</div>
 	</div>
 </div>

 <?php $this->load->view("user/side/footer"); ?>
 <script>
 	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
 		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
 		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
 	})(window,document,'script','../../../../../www.google-analytics.com/analytics.js','ga');
 	ga('create', 'UA-64667612-1', 'themehats.com');
 	ga('send', 'pageview');
 </script>
 <?php $this->load->view("user/side/js"); ?>