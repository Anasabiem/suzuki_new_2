 <?php $this->load->view("user/side/head"); ?>
 <?php $this->load->view("user/side/navbar"); ?>
 <div class="c-layout-page">
 	<!-- BEGIN: LAYOUT/BREADCRUMBS/BREADCRUMBS-2 -->
 	<div class="c-layout-breadcrumbs-1 c-subtitle c-fonts-uppercase c-fonts-bold c-bordered c-bordered-both">
 		<div class="container">
 			<div class="c-page-title c-pull-left">
 				<h3 class="c-font-uppercase c-font-sbold">Details Produk</h3>
 				<h4 class=""></h4>
 			</div>
 			<ul class="c-page-breadcrumbs c-theme-nav c-pull-right c-fonts-regular">
 				<li><a href="shop-product-details-2.html">Home</a></li>
 				<li>/</li>
 				<li class="c-state_active">Details Produk</li>

 			</ul>
 		</div>
 	</div><!-- END: LAYOUT/BREADCRUMBS/BREADCRUMBS-2 -->
 	<!-- BEGIN: PAGE CONTENT -->
 	<!-- BEGIN: CONTENT/SHOPS/SHOP-PRODUCT-DETAILS-2 -->
 	<div class="c-content-box c-size-lg c-overflow-hide c-bg-white">
 		<div class="container">
 			<?php foreach ($detail->result() as $detail): ?>
 				<div class="c-shop-product-details-2">
 					<div class="row">
 						<div class="col-md-6">
 							<div class="c-product-gallery">
 								<div class="c-product-gallery-content" style="height: 300px;">
 									<div class="">
 										<img height="300" src="<?php echo $detail->foto ?>">
 									</div>
 								</div>
 							</div>
 						</div>
 						<div class="col-md-6">
 							<div class="c-product-meta">
 								<div class="c-content-title-1">
 									<h3 class="c-font-uppercase c-font-bold"><?php echo $detail->nmProduct ?></h3>
 									<div class="c-line-left"></div>
 								</div>
 								<div class="c-product-badge">
 									<?php if ($detail->kategori=='promo'){ ?>
								<div class="c-product-sale">Promo</div>
							<?php } elseif ($detail->kategori=='baru') { ?>
								<div class="c-product-new">Baru</div>
							<?php } elseif($detail->kategori=='pb'){ ?>
								<div class="c-product-new">Promo</div>
								<div class="c-product-new">Baru</div>
							<?php } else{ ?>
								
							<?php } ?>
 								</div> <br> <br>
 								<div class="c-product-review">
 								</div>
 								<div class="c-product-price">Rp. <?php echo number_format($detail->harga) ?></div>
 								<div class="c-product-short-desc">
 									<?php echo $detail->detail ?>
 								</div>
 								<div class="c-product-add-cart c-margin-t-20">
 									<div class="row">
 									<div class="col-sm-12 col-xs-12 c-margin-t-20">
 										<a href="https://api.whatsapp.com/send?phone=6282230033283&text=Halo%20saya%20mau%20beli%20<?php echo $detail->nmProduct ?>%20sesuai%20harga%20di%20Web"><button class="btn c-btn btn-lg c-font-bold c-font-white c-theme-btn c-btn-square c-font-uppercase">Pesan Sekarang</button></a>
 									</div>
 								</div>
 							</div>
 						</div>
 					</div>
 				</div>
 			</div>
 		<?php endforeach ?>
 	</div>
 </div><!-- END: CONTENT/SHOPS/SHOP-PRODUCT-DETAILS-2 -->
 <!-- BEGIN: CONTENT/SHOPS/SHOP-PRODUCT-TAB-1 -->
<div class="c-content-box c-size-md c-no-padding">
	<div class="c-shop-product-tab-1" role="tabpanel">
		<div class="container">
			<ul class="nav nav-justified" role="tablist">
				<li role="presentation" >
					<a class="c-font-uppercase c-font-bold" href="" role="tab" data-toggle="tab">_______________</a>
				</li>
				<li role="presentation" class="active">
					<a class="c-font-uppercase c-font-bold" href="#tab-2" role="tab" data-toggle="tab">Detail Harga Semua Tipe</a>
				</li>
				<li role="presentation">
					<a class="c-font-uppercase c-font-bold" href="" role="tab" data-toggle="tab">_______________</a>
				</li>
			</ul>
		</div>
		<div class="tab-content">
			<div role="tabpanel" class="tab-pane fade in active" id="tab-2">
				<div class="container" style="padding: 20px;">
					<table class="table table-bordered" style="text-align: center; padding: 20px">
 											<thead>
 												<tr>
 													<th>No</th>
 													<th>Type</th>
 													<th>Harga</th>
 												</tr>
 											</thead>
 											<?php $no=1; foreach ($harga->result() as $ty): ?>
 											<tbody>
 												<th><?php echo $no++; ?></th>
 												<th><?php echo $ty->tipe ?></th>
 												<th>Rp. <?php echo number_format($ty->harganya) ?></th>
 											</tbody>
 										<?php endforeach ?>
 									</table>
					<!-- <p class="c-center"><strong>Colors:</strong> Red, Black, Beige, White</p><br/> -->
				</div>
			</div>
		</div>
	</div>
</div><!-- END: CONTENT/SHOPS/SHOP-PRODUCT-TAB-1 -->

 <!-- END: PAGE CONTENT -->
</div>
<?php $this->load->view("user/side/footer"); ?>
<script src="<?php echo base_url()?>master/client/assets/plugins/zoom-master/jquery.zoom.min.js" type="text/javascript"></script>
<!-- END: PAGE SCRIPTS -->
<!-- END: LAYOUT/BASE/BOTTOM -->
<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','<?php echo base_url()?>master/client/www.google-analytics.com/analytics.js','ga');
	ga('create', 'UA-64667612-1', 'themehats.com');
	ga('send', 'pageview');
</script>
<?php $this->load->view("user/side/js"); ?>