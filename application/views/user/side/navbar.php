 <header class="c-layout-header c-layout-header-4 c-layout-header-default-mobile" data-minimize-offset="80">
    <div class="c-navbar">
        <div class="container">
            <!-- BEGIN: BRAND -->
            <div class="c-navbar-wrapper clearfix">
                <div class="c-brand c-pull-left">
                    <a href="<?php echo base_url() ?>" class="c-logo">
                        <img src="<?php echo base_url() ?>logo.png" height="50" class="c-desktop-logo">
                        <img src="<?php echo base_url() ?>logo.png" height="50" class="c-desktop-logo-inverse">
                        <img src="<?php echo base_url() ?>logo.png" height="50" class="c-mobile-logo"> </a>
                        <button class="c-hor-nav-toggler" type="button" data-target=".c-mega-menu">
                            <span class="c-line"></span>
                            <span class="c-line"></span>
                            <span class="c-line"></span>
                        </button>
                        <button class="c-topbar-toggler" type="button">
                            <i class="fa fa-ellipsis-v"></i>
                        </button>
                        <button class="c-search-toggler" type="button">
                            <i class="fa fa-search"></i>
                        </button>
                    </div>
                    <!-- END: BRAND -->
                    <!-- BEGIN: QUICK SEARCH -->
                    <form class="c-quick-search" action="#">
                        <input type="text" name="query" placeholder="Type to search..." value="" class="form-control" autocomplete="off">
                        <span class="c-theme-link">&times;</span>
                    </form>
                    <!-- END: QUICK SEARCH -->
                    <!-- BEGIN: HOR NAV -->
                    <!-- BEGIN: LAYOUT/HEADERS/MEGA-MENU -->
                    <!-- BEGIN: MEGA MENU -->
                    <!-- Dropdown menu toggle on mobile: c-toggler class can be applied to the link arrow or link itself depending on toggle mode -->
                    <nav class="c-mega-menu c-pull-right c-mega-menu-dark c-mega-menu-dark-mobile c-fonts-uppercase c-fonts-bold">
                        <ul class="nav navbar-nav c-theme-nav">
                            <li>
                                <a href="<?php echo base_url() ?>" class="c-link dropdown-toggle">Home
                                    <span class="c-arrow c-toggler"></span>
                                </a>
                            </li>
                            <li class="c-menu-type-classic">
                                <a href="<?php echo base_url('Product') ?>" class="c-link dropdown-toggle">Product
                                    <span class="c-arrow c-toggler"></span>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo base_url('Galery') ?>" class="c-link dropdown-toggle">Gallery
                                    <span class="c-arrow c-toggler"></span>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo base_url('ContactUs') ?>" class="c-link dropdown-toggle">Contact
                                    <span class="c-arrow c-toggler"></span>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo base_url('LoginAdmin') ?>" class="c-btn-border-opacity-04 c-btn btn-no-focus c-btn-header btn btn-sm c-btn-border-1x c-btn-dark c-btn-circle c-btn-uppercase c-btn-sbold"><i class="icon-user"></i> Sign In</a>
                            </li>
                            <li class="c-quick-sidebar-toggler-wrapper">
                                <a href="#" class="c-quick-sidebar-toggler">
                                    <span class="c-line"></span>
                                    <span class="c-line"></span>
                                    <span class="c-line"></span>
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="c-cart-menu">
                    <div class="c-cart-menu-title">
                        <p class="c-cart-menu-float-l c-font-sbold">2 item(s)</p>
                        <p class="c-cart-menu-float-r c-theme-font c-font-sbold">$79.00</p>
                    </div>
                    <ul class="c-cart-menu-items">
                        <li>
                            <div class="c-cart-menu-close">
                                <a href="#" class="c-theme-link">×</a>
                            </div>
                            <img src="assets/base/img/content/shop2/24.jpg" />
                            <div class="c-cart-menu-content">
                                <p>1 x
                                    <span class="c-item-price c-theme-font">$30</span>
                                </p>
                                <a href="index626f.html?p=shop-product-details-2" class="c-item-name c-font-sbold">Winter Coat</a>
                            </div>
                        </li>
                        <li>
                            <div class="c-cart-menu-close">
                                <a href="#" class="c-theme-link">×</a>
                            </div>
                            <img src="assets/base/img/content/shop2/12.jpg" />
                            <div class="c-quick-sidebar-toggler">
                                <p>1 x
                                    <span class="c-item-price c-theme-font">$30</span>
                                </p>
                                <a href="index4d7b.html?p=shop-product-details" class="c-item-name c-font-sbold">Sports Wear</a>
                            </div>
                        </li>
                    </ul>
                    <div class="c-cart-menu-footer">
                        <a href="index6617.html?p=shop-cart" class="btn btn-md c-btn c-btn-square c-btn-grey-3 c-font-white c-font-bold c-center c-font-uppercase">View Cart</a>
                        <a href="indexbfa8.html?p=shop-checkout" class="btn btn-md c-btn c-btn-square c-theme-btn c-font-white c-font-bold c-center c-font-uppercase">Checkout</a>
                    </div>
                </div>
            </div>
        </div>
    </header>