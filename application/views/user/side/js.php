<script src="<?php echo base_url() ?>master/client/assets/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>master/client/assets/plugins/jquery-migrate.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>master/client/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>master/client/assets/plugins/jquery.easing.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>master/client/assets/plugins/reveal-animate/wow.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>master/client/assets/demos/index/js/scripts/reveal-animate/reveal-animate.js" type="text/javascript"></script>
        <!-- END: CORE PLUGINS -->
        <!-- BEGIN: LAYOUT PLUGINS -->
        <script src="<?php echo base_url() ?>master/client/assets/plugins/revo-slider/js/jquery.themepunch.tools.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>master/client/assets/plugins/revo-slider/js/jquery.themepunch.revolution.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>master/client/assets/plugins/revo-slider/js/extensions/revolution.extension.slideanims.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>master/client/assets/plugins/revo-slider/js/extensions/revolution.extension.layeranimation.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>master/client/assets/plugins/revo-slider/js/extensions/revolution.extension.navigation.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>master/client/assets/plugins/revo-slider/js/extensions/revolution.extension.video.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>master/client/assets/plugins/revo-slider/js/extensions/revolution.extension.parallax.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>master/client/assets/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>master/client/assets/plugins/owl-carousel/owl.carousel.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>master/client/assets/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>master/client/assets/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>master/client/assets/plugins/fancybox/jquery.fancybox.pack.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>master/client/assets/plugins/smooth-scroll/jquery.smooth-scroll.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>master/client/assets/plugins/typed/typed.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>master/client/assets/plugins/slider-for-bootstrap/js/bootstrap-slider.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>master/client/assets/plugins/js-cookie/js.cookie.js" type="text/javascript"></script>
        <!-- END: LAYOUT PLUGINS -->
        <!-- BEGIN: THEME SCRIPTS -->
        <script src="<?php echo base_url() ?>master/client/assets/base/js/components.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>master/client/assets/base/js/components-shop.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>master/client/assets/base/js/app.js" type="text/javascript"></script>
        <script>
            $(document).ready(function()
            {
                App.init(); // init core    
            });
        </script>
        <!-- END: THEME SCRIPTS -->
        <!-- BEGIN: PAGE SCRIPTS -->
        <script src="<?php echo base_url() ?>master/client/assets/demos/default/js/scripts/revo-slider/slider-4.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>master/client/assets/plugins/isotope/isotope.pkgd.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>master/client/assets/plugins/isotope/imagesloaded.pkgd.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>master/client/assets/plugins/isotope/packery-mode.pkgd.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>master/client/assets/plugins/ilightbox/js/jquery.requestAnimationFrame.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>master/client/assets/plugins/ilightbox/js/jquery.mousewheel.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>master/client/assets/plugins/ilightbox/js/ilightbox.packed.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>master/client/assets/demos/default/js/scripts/pages/isotope-gallery.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>master/client/assets/plugins/revo-slider/js/extensions/revolution.extension.parallax.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>master/client/assets/plugins/revo-slider/js/extensions/revolution.extension.kenburn.min.html" type="text/javascript"></script>
        <!-- END: PAGE SCRIPTS -->
        <!-- END: LAYOUT/BASE/BOTTOM -->
        <script src="<?php echo base_url() ?>master/client/assets/demos/default/js/scripts/pages/masonry-gallery.js" type="text/javascript"></script>