<!DOCTYPE html>
<html lang="en"  >
<head>
    <meta charset="utf-8"/>
    <title>Suzuki</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">
    <meta content="" name="description"/>
    <meta content="" name="author"/>
    <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:300italic,400italic,700italic,400,300,700&amp;subset=all' rel='stylesheet' type='text/css'>
    <link href="<?php echo base_url()?>master/client/assets/plugins/socicon/socicon.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url()?>master/client/assets/plugins/bootstrap-social/bootstrap-social.css" rel="stylesheet" type="text/css"/>      
    <link href="<?php echo base_url()?>master/client/assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url()?>master/client/assets/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url()?>master/client/assets/plugins/animate/animate.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url()?>master/client/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url()?>master/client/assets/plugins/revo-slider/css/settings.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url()?>master/client/assets/plugins/revo-slider/css/layers.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url()?>master/client/assets/plugins/revo-slider/css/navigation.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url()?>master/client/assets/plugins/cubeportfolio/css/cubeportfolio.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url()?>master/client/assets/plugins/owl-carousel/assets/owl.carousel.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url()?>master/client/assets/plugins/fancybox/jquery.fancybox.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url()?>master/client/assets/plugins/slider-for-bootstrap/css/slider.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url()?>master/client/assets/demos/default/css/plugins.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url()?>master/client/assets/demos/default/css/components.css" id="style_components" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url()?>master/client/assets/demos/default/css/themes/default.css" rel="stylesheet" id="style_theme" type="text/css"/>
    <link href="<?php echo base_url()?>master/client/assets/demos/default/css/custom.css" rel="stylesheet" type="text/css"/>
    <link rel="shortcut icon" href="favicon.ico"/>
</head>
<body class="c-layout-header-fixed c-layout-header-mobile-fixed c-layout-header-fullscreen"> 
    <?php $this->load->view('user/side/navbar') ?>
    <div class="c-layout-page">
        <section class="c-layout-revo-slider c-layout-revo-slider-7" dir="ltr">
            <div class="tp-banner-container tp-fullscreen tp-fullscreen-mobile">
                <div class="c-singup-form">
                    <h3 class="c-font-54 c-font-thin c-font-white c-margin-b-40 c-font-uppercase">
                        UMC <span class="c-theme-font c-font-bold">SUZUKI</span> JEMBER
                    </h3>

                    <h5 class="c-font-24 c-font-thin c-font-uppercase c-font-white c-subtitle c-margin-b-40">
                        DAPATKAN DISKON DAN PROMO HANYA DI SINI
                    </h5>

                    <form class="form-inline" action="#">
                        <button type="submit" class="btn btn-lg btn-danger c-btn-uppercase c-btn-square c-btn-bold">Hubungi Kami</button>
                    </form>
                </div>
                <div class="tp-banner rev_slider" data-version="5.0">
                    <ul>
                        <!--BEGIN: SLIDE #1 -->
                        <li data-transition="fade" data-slotamount="1" data-masterspeed="1000" data-style="dark">           
                            <img 
                            alt="" 
                            src="https://ik.imagekit.io/hj8sm3kk7/large/gallery/exterior/37/769/suzuki-baleno-front-angle-low-view-824765.jpg"
                            data-bgposition="center center" 
                            data-bgfit="cover" 
                            data-bgrepeat="no-repeat"
                            >   
                        </li>
                        <!--END --> 

                        <!--BEGIN: SLIDE #2 -->
                        <li data-transition="fade" data-slotamount="1" data-masterspeed="1000" data-style="dark">           
                            <img 
                            alt="" 
                            src="https://kreditmobilbekasi.com/wp-content/uploads/2018/02/Daftar-Harga-Kredit-Mobil-suzuki-Ertiga-Terbaru-DP-Murah-di-Bekasi.jpg"
                            data-bgposition="center center" 
                            data-bgfit="cover" 
                            data-bgrepeat="no-repeat"
                            >   
                        </li>
                        <!--END -->
                    </ul>
                </div>
            </div>
        </section>

        <!-- BEGIN: CONTENT/MISC/SERVICES-3 -->
        <div class="c-content-box c-size-md c-bg-white">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="c-content-title-1 c-margin-b-60">
                            <h3 class="c-center c-font-uppercase c-font-bold">
                                How We Optimize Your Business
                            </h3>                   
                            <div class="c-line-center"></div>
                            <p class="c-center c-font-uppercase c-font-17">
                                Lihat, Sesuaikan, Pesan
                            </p>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4 col-sm-6 wow animate fadeInLeft">
                        <div class="c-content-step-1 c-opt-1">
                            <div class="c-icon"><span class="c-hr c-hr-first"><span class="c-content-line-icon c-icon-14 c-theme"></span></span></div>
                            <div class="c-title c-font-20 c-font-bold c-font-uppercase">1. First Important STEP</div>
                            <div class="c-description c-font-17">
                                Konsultasikan Kebutuhan anda
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 wow animate fadeInLeft" data-wow-delay="0.2s">
                        <div class="c-content-step-1 c-opt-1">
                            <div class="c-icon"><span class="c-hr"><span class="c-content-line-icon c-icon-21 c-theme"></span></span></div>
                            <div class="c-title c-font-20 c-font-bold c-font-uppercase">2. Second Phase</div>
                            <div class="c-description c-font-17">
                                Lihat Barang dan sesuaikan dengan budged anda
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-12 wow animate fadeInLeft" data-wow-delay="0.4s">
                        <div class="c-content-step-1 c-opt-1">
                            <div class="c-icon"><span class="c-hr c-hr-last"><span class="c-content-line-icon c-icon-32 c-theme"></span></span></div>
                            <div class="c-title c-font-20 c-font-bold c-font-uppercase">3. Final Action</div>
                            <div class="c-description c-font-17">
                                Pesan
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="c-content-box c-size-md c-bg-grey-1">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">      
                        <div class="c-content-media-2-slider" data-slider="owl">
                            <div class="c-content-label c-font-uppercase c-font-bold">Terbaru di SUZUKI</div>                     
                            <div class="owl-carousel owl-theme c-theme owl-single" data-single-item="true" data-auto-play="4000" data-rtl="false">
                                <?php $no=1; foreach ($produk as $key): ?>
                                <div class="item">
                                    <div class="c-content-media-2 c-bg-img-center" style="background-image: url(<?php echo $key->foto ?>); min-height: 380px;">
                                        <div class="c-panel">
                                            <div class="c-fav">
                                                <i class="icon-heart c-font-thin"></i>
                                                <p class="c-font-thin"></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php endforeach ?>
                            </div>
                        </div>      
                    </div>
                    <div class="col-md-4">
                        <div class="c-content-media-1" style="height: 380px;">
                            <div class="c-content-label c-font-uppercase c-font-bold c-theme-bg">Sales Man Profile</div>
                            <a href="#" class="c-title c-font-uppercase  c-font-bold c-theme-on-hover">Afir suzuki</a>
                            <p>Whatsapp : <a href="https://wa.me/+6282230033283"><i>+6282230033283</i></a><br>E-mail : Afircenk14@gmail.com</p>
                            <div class="c-author">
                                <div class="c-portrait" style="background-image: url(<?php echo base_url()?>master/df.jpeg)"></div>
                                <div class="c-name c-font-uppercase">Afir suzuki</div>
                            </div>
                        </div>
                    </div>          
                </div>
            </div>
        </div>
        <!-- END: CONTENT/FEATURES/FEATURES-15-1 -->

        <!-- BEGIN: CONTENT/APPS/APP-2 -->
        <!-- BEGIN: PARALLAX 1 - OPTION 4 -->
        <div class="c-content-box c-size-md c-bg-parallax" style="background-image: url(<?php echo base_url()?>master/client/assets/base/img/content/backgrounds/bg-85.jpg)">
            <div class="container">
                <div class="c-content-app-1">
                    <!-- Begin: Title 1 component -->
                    <div class="c-content-title-1">
                        <h3 class="c-font-uppercase c-center c-font-bold">Daftar Harga Mobil Suzuki</h3>
                        <div class="c-line-center c-theme-bg"></div>
                    </div>
                    <!-- End-->
                    <div class="c-diagram">
                        <div class="c-lines-2" style="background-image: url(<?php echo base_url()?>master/client/assets/base/img/content/apps/app-line2.png)"></div>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Product</th>
                                    <th>Type</th>
                                    <th>Harga</th>
                                </tr>
                            </thead>
                            <?php $no=1; foreach ($harga as $key): ?>
                            <tbody>
                                <tr>
                                    <th><?php echo $no++; ?></th>
                                    <th><?php echo $key->nmProduct ?></th>
                                    <th><?php echo $key->tipe ?></th>
                                    <th>Rp. <?php echo number_format($key->harganya) ?></th>
                                </tr>
                            </tbody>
                        <?php endforeach ?>
                    </table>
                </div>          
            </div>
        </div> 
    </div>
    <!-- END: PARALLAX 1 - OPTION 4 -->
    <!-- END: CONTENT/APPS/APP-2 -->
</div>
<!-- END: PAGE CONTAINER -->

<!-- BEGIN: LAYOUT/FOOTERS/FOOTER-6 -->
<a name="footer"></a>
<div class="c-content-box c-size-md c-bg-white">
    <div class="container">
        <div class="c-content-bar-2 c-opt-1">
            <div class="row" data-auto-height="true">
                <div class="col-md-6">
                    <!-- Begin: Title 1 component -->
                    <div class="c-content-title-1" data-height="height">
                        <h3 class="c-font-uppercase c-font-bold">AYO BELI MOBIL SUZUKI</h3>
                        <p class="c-font-uppercase c-font-sbold">
                            Kami siap melayani anda dengan sepenuh hati
                        </p>
                        <a href="" class="btn btn-md c-btn-border-2x c-btn-square c-theme-btn c-btn-uppercase c-btn-bold">Pesan Sekarang</a>
                    </div>
                    <!-- End-->
                </div>
                <div class="col-md-6">
                    <div class="c-content-v-center c-bg-red" data-height="height">
                        <div class="c-wrapper">
                            <div class="c-body">
                                <h3 class="c-font-white c-font-bold">AYO BELI SEKARANG, ATAU TIDAK SAMA SEKALI</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> 
</div>
<?php $this->load->view('user/side/footer') ?>
<!-- END: LAYOUT/FOOTERS/FOOTER-6 -->

<!-- BEGIN: LAYOUT/FOOTERS/GO2TOP -->
<div class="c-layout-go2top">
    <i class="icon-arrow-up"></i>
</div><!-- END: LAYOUT/FOOTERS/GO2TOP -->

<!-- BEGIN: LAYOUT/BASE/BOTTOM -->
<!-- BEGIN: CORE PLUGINS -->
    <!--[if lt IE 9]>
    <script src="<?php echo base_url()?>master/client/assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
<script src="<?php echo base_url()?>master/client/assets/plugins/jquery.min.js" type="text/javascript" ></script>
<script src="<?php echo base_url()?>master/client/assets/plugins/jquery-migrate.min.js" type="text/javascript" ></script>
<script src="<?php echo base_url()?>master/client/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript" ></script>
<script src="<?php echo base_url()?>master/client/assets/plugins/jquery.easing.min.js" type="text/javascript" ></script>
<script src="<?php echo base_url()?>master/client/assets/plugins/reveal-animate/wow.js" type="text/javascript" ></script>
<script src="<?php echo base_url()?>master/client/assets/demos/default/js/scripts/reveal-animate/reveal-animate.js" type="text/javascript" ></script>

<!-- END: CORE PLUGINS -->

<!-- BEGIN: LAYOUT PLUGINS -->
<script src="<?php echo base_url()?>master/client/assets/plugins/revo-slider/js/jquery.themepunch.tools.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>master/client/assets/plugins/revo-slider/js/jquery.themepunch.revolution.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>master/client/assets/plugins/revo-slider/js/extensions/revolution.extension.slideanims.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>master/client/assets/plugins/revo-slider/js/extensions/revolution.extension.layeranimation.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>master/client/assets/plugins/revo-slider/js/extensions/revolution.extension.navigation.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>master/client/assets/plugins/revo-slider/js/extensions/revolution.extension.video.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>master/client/assets/plugins/revo-slider/js/extensions/revolution.extension.parallax.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>master/client/assets/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>master/client/assets/plugins/owl-carousel/owl.carousel.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>master/client/assets/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>master/client/assets/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>master/client/assets/plugins/fancybox/jquery.fancybox.pack.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>master/client/assets/plugins/smooth-scroll/jquery.smooth-scroll.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>master/client/assets/plugins/typed/typed.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>master/client/assets/plugins/slider-for-bootstrap/js/bootstrap-slider.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>master/client/assets/plugins/js-cookie/js.cookie.js" type="text/javascript"></script>
<!-- END: LAYOUT PLUGINS -->

<!-- BEGIN: THEME SCRIPTS -->
<script src="<?php echo base_url()?>master/client/assets/base/js/components.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>master/client/assets/base/js/components-shop.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>master/client/assets/base/js/app.js" type="text/javascript"></script>
<script>
    $(document).ready(function() {    
        App.init(); // init core    
    });
</script>
<!-- END: THEME SCRIPTS -->

<!-- BEGIN: PAGE SCRIPTS -->
<script src="<?php echo base_url()?>master/client/assets/plugins/revo-slider/js/extensions/revolution.extension.kenburn.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>master/client/assets/plugins/revo-slider/js/extensions/revolution.extension.parallax.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>master/client/assets/demos/default/js/scripts/revo-slider/slider-13.js" type="text/javascript"></script>
<!-- END: PAGE SCRIPTS -->
<!-- END: LAYOUT/BASE/BOTTOM -->
<script>
    $(document).ready(function() {
        var slider = $('.c-layout-revo-slider .tp-banner');
        var cont = $('.c-layout-revo-slider .tp-banner-container');

        var api = slider.show().revolution({
            sliderType:"standard",
            sliderLayout:"fullscreen",
            responsiveLevels:[2048,1024,778,480],
            gridwidth: [1170, 1024, 778, 480],
            gridheight: [620, 768, 960, 720],
            delay: 15000,    
            startwidth:1170,
            startheight: 620,
            
            touchenabled: "on",

            navigation: {
                keyboardNavigation:"off",
                keyboard_direction: "horizontal",
                mouseScrollNavigation:"off",
                onHoverStop:"on",
                arrows: {
                    style:"circle",
                    enable:true,
                    hide_onmobile:false,
                    hide_onleave:false,
                    tmp:'',
                    left: {
                        h_align:"left",
                        v_align:"center",
                        h_offset:30,
                        v_offset:0
                    },
                    right: {
                        h_align:"right",
                        v_align:"center",
                        h_offset:30,
                        v_offset:0
                    }
                }           
            },

            spinner: "spinner2",

            shadow: 0,
            fullWidth: "off",
            forceFullWidth: "off",
            disableProgressBar:"on",

            hideThumbsOnMobile: "on",
            hideNavDelayOnMobile: 1500,
            hideBulletsOnMobile: "on",
            hideArrowsOnMobile: "on",
            hideThumbsUnderResolution: 0
        });

        api.bind("revolution.slide.onchange",function (e,data) {

            $('.c-layout-header').removeClass('hide');   

            setTimeout(function(){
                $('.c-singup-form').fadeIn(); 
            }, 1500);
        });
}); //ready 
</script>
</body>


<!-- Mirrored from themehats.com/themes/jango/demos/default/home-13.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 16 Sep 2018 12:21:54 GMT -->
</html>