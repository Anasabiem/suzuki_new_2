<?php $this->load->view('admin/side/head') ?>
<?php $this->load->view('admin/side/navbar') ?>
<div class="container-fluid page-body-wrapper">
	<div class="main-panel">
		<div class="content-wrapper">
			<div class="row">
				<div class="col-sm-6 mb-4 mb-xl-0">
					<div class="d-lg-flex align-items-center">
						<div>
							<h3 class="text-dark font-weight-bold mb-2">Hi, welcome back!</h3>
							<h6 class="font-weight-normal mb-2">Last login was 23 hours ago. View details</h6>
						</div>
						<div class="ml-lg-5 d-lg-flex d-none">
							<button type="button" class="btn bg-white btn-icon">
								<i class="mdi mdi-view-grid text-success"></i>
							</button>
							<button type="button" class="btn bg-white btn-icon ml-2">
								<i class="mdi mdi-format-list-bulleted font-weight-bold text-primary"></i>
							</button>
						</div>
					</div>
				</div>
			</div>
			<div class="row mt-4">
				<div class="col-lg-12 grid-margin stretch-card">
					<div class="card">
						<div class="card-body">
							<div class="row">
								Slider Home
							</div>
						</div>
					</div>
				</div>
				<?php $no=1; foreach ($galeri->result() as $key): ?>
					<div class="col-lg-4 mb-3 mb-lg-0">
					<div class="card congratulation-bg text-center">
						<div class="card-body pb-0">
							<img height="200px;" width="250px;" src="<?php echo  base_url()?>master/admin/images/dashboard/<?php echo $key->foto; ?>" alt="">  
							<h2 class="mt-3 text-white mb-3 font-weight-bold">Slider <?php echo $no++; ?></h2>
							<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal<?php echo $key->idGaleri ?>">Edit Slider Foto</button>
								<div id="myModal<?php echo $key->idGaleri ?>" class="modal fade" role="dialog">
									<div class="modal-dialog">

										<!-- Modal content-->
										<form method="post" action="">
										<div class="modal-content">
											<div class="modal-header">
												<h4 class="modal-title">Modal Header</h4>
												<button type="button" class="close" data-dismiss="modal">&times;</button>
											</div>
											<div class="modal-body">
												<input type="hidden" name="id" value="<?php echo $key->idGaleri ?>">
												<input type="file" name="gambar_slider" required="">
											</div>
											<div class="modal-footer">
												<button type="submit" class="btn btn-info btn-lg" data-dismiss="modal">Update</button>
											</div>
										</div>
										</form>
									</div>
								</div>
						</div>

					</div>
				</div>
				<?php endforeach ?>
			</div>

		</div>
		<!-- content-wrapper ends -->
		<!-- partial:partials/_footer.html -->
		<footer class="footer">
			<div class="footer-wrap">
				<div class="w-100 clearfix">
					<span class="d-block text-center text-sm-left d-sm-inline-block">Copyright © 2018 <a href="https://www.templatewatch.com/" target="_blank">templatewatch</a>. All rights reserved.</span>
					<span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with <i class="mdi mdi-heart-outline"></i></span>
				</div>
			</div>
		</footer>
		<!-- partial -->
	</div>
	<!-- main-panel ends -->
</div>
<?php $this->load->view('admin/side/js') ?>