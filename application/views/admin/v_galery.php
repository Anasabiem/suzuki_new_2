<?php $this->load->view('admin/side/head') ?>
<?php $this->load->view('admin/side/navbar') ?>
<br><br><hr>
<div class="col-lg-12 grid-margin stretch-card">
  <div class="card">
    <div class="card-body">
      <div class="row">
        <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Tambah Data</button>
        <div id="myModal" class="modal fade" role="dialog">
          <div class="modal-dialog">

            <!-- Modal content-->
            <form autocomplete="off" method="post" enctype="multipart/form-data" action="<?php echo base_url('admin/Galery/create') ?>" >
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title">Galery</h4>
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                  <div class="form-group">
                    <label for="exampleInputUsername1">Nama Foto</label>
                    <input type="text" class="form-control" id="exampleInputUsername1" placeholder="Nama Foto" name="nmfoto">
                  </div>
                  <div class="form-group">
                      <label>File upload</label>
                      <input type="file" name="pilih" class="file-upload-default">
                      <div class="input-group col-xs-12">
                        <input type="text" class="form-control file-upload-info" name="pilih" required="" disabled placeholder="Upload Image">
                        <span class="input-group-append">
                          <button class="file-upload-browse btn btn-primary" name="gambar_galeri" type="button">Pilih File</button>
                        </span>
                      </div>
                    </div>
                </div>
                <div class="modal-footer">
                  <button name="btnSimpan" value="Simpan" type="submit" class="btn btn-info btn-lg">Tambah</button>
                </div>
              </div>
            <?php echo form_close(); ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="col-lg-12 grid-margin stretch-card">
  <div class="card">
    <div class="card-body">
      <h4 class="card-title">Galeri</h4>
      <p class="card-description">
        UMC  <code>Suzuki Jember</code>
      </p>
      <div class="table-responsive pt-3">
        <table class="table table-bordered">
          <thead>
            <tr>
              <th>
                No
              </th>
              <th>Nama</th>
              <th>url</th>
              <th>Foto</th>
              <th>Aksi</th>
            </tr>
          </thead>
          <?php $no=1; foreach ($galeri->result() as $key): ?>
            <tbody>
            <tr>
              <td><?php echo $no++; ?></td>
              <td>
                <?php echo $key->nama; ?>
              </td>
              <td>
                <?php echo $key->foto ?>
              </td>
              <td>
                <img src="<?php echo  base_url()?><?php echo $key->foto; ?>">
              </td>
              <td>
                <a href="<?php echo base_url('admin/Galery/hapus/'.$key-> idGaleri) ?>" class="btn btn-danger waves-effect " title="Hapus" onclick="javascript: return confirm('Anda Yakin Akan Menghapus ?')"><i class="mdi mdi-delete"></i></a>
              </td>
            </tr>

        </tbody>
          <?php endforeach ?>
      </table>
      <br><br><br><hr>
      <br><br>
    </div>
  </div>
</div>
</div>
<?php $this->load->view('admin/side/js') ?>