<?php $this->load->view('admin/side/head') ?>
<?php $this->load->view('admin/side/navbar') ?>
<br><br><hr>
<div class="col-lg-12 grid-margin stretch-card">
  <div class="card">
    <div class="card-body">
      <div class="row">
        <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Tambah Data</button>
        <div id="myModal" class="modal fade" role="dialog">
          <div class="modal-dialog">

            <!-- Modal content-->
            <form method="post" action="<?php echo base_url('admin/Product/tambah') ?>" enctype="multipart/form-data">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title">Modal Header</h4>
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                  <div class="form-group">
                    <label for="exampleInputUsername1">Nama Product</label>
                    <input type="text" class="form-control" id="exampleInputUsername1" placeholder="Username" name="nm_product">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputUsername1">Url Foto Product</label>
                    <input type="text" class="form-control" id="exampleInputUsername1" placeholder="Url" name="url">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputUsername1">Harga</label>
                    <input type="text" class="form-control" id="exampleInputUsername1" placeholder="Harga" name="harga">
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect2">Kategori</label>
                    <select class="form-control" id="exampleFormControlSelect2" name="kategori">
                      <option>Biasa</option>
                      <option>Baru</option>
                      <option>Promo</option>
                      <option value="bp">Baru dan Promo</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputUsername1">Detail Product</label>
                    <textarea type="text" class="form-control" id="exampleInputUsername1" placeholder="Username" name="details"></textarea>
                  </div>
                </div>
                <div class="modal-footer">
                  <button name="selesai" type="submit" class="btn btn-info btn-lg">Tambah</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="col-lg-12 grid-margin stretch-card">
  <div class="card">
    <div class="card-body">
      <h4 class="card-title">Tabel product</h4>
      <p class="card-description">
        UMC  <code>Suzuki Jember</code>
      </p>
      <div class="table-responsive pt-3">
        <table class="table table-bordered">
          <thead>
            <tr>
              <th>
                No
              </th>
              <th>Nama Product</th>
              <th>Kategori</th>
              <th>Url</th>
              <th>Harga</th>
              <th>Detalil</th>
              <th>Aksi</th>
            </tr>
          </thead>
          <tbody>
            <?php $no = 1; foreach ($gambar as $key): ?>
            <tr>
              <td>
                <?php echo $no++; ?>
              </td>
              <td>
                <?php echo $key->nmProduct ?>
              </td>
              <td>
                <?php echo $key->kategori ?>
              </td>
              <td>
                <?php echo $key->foto ?>
              </td>
              <td>
                Rp. <?php echo number_format($key->harga) ?>
              </td>
              <td>
                <?php echo $key->detail ?>
              </td>
              <td>
                <button type="button" class="btn btn-info waves-effectg" data-toggle="modal" data-target="#myModal1<?php echo $key->idProduct  ?>"><i class="mdi mdi-grease-pencil"></i></button>
                <a href="<?php echo base_url('admin/Product/hapus/'.$key-> idProduct) ?>" class="btn btn-danger waves-effect " title="Hapus" onclick="javascript: return confirm('Anda Yakin Akan Menghapus ?')"><i class="mdi mdi-delete"></i></a>
                <div id="myModal1<?php echo $key->idProduct?>" class="modal fade" role="dialog">
                  <div class="modal-dialog">

                    <!-- Modal content-->
                    <form method="post" action="<?php echo base_url('admin/Product/update') ?>" enctype="multipart/form-data">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h4 class="modal-title">Modal Header</h4>
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body">
                          <div class="form-group">
                            <label for="exampleInputUsername1">Nama Product</label>
                            <input type="hidden" name="idP" value="<?php echo $key->idProduct ?>">
                            <input type="text" class="form-control" id="exampleInputUsername1" placeholder="Username" name="nm_product" value="<?php echo $key->nmProduct ?>">
                          </div>
                          <div class="form-group">
                            <label for="exampleInputUsername1">Url Foto Product</label>
                            <input type="text" class="form-control" id="exampleInputUsername1" placeholder="url" name="url" value="<?php echo $key->foto ?>">
                          </div>
                          <div class="form-group">
                            <label for="exampleInputUsername1">Harga</label>
                            <input type="text" class="form-control" id="exampleInputUsername1" placeholder="Username" name="harga" value="<?php echo $key->harga ?>">
                          </div>
                          <div class="form-group">
                            <label for="exampleFormControlSelect2">Kategori</label>
                            <select class="form-control" id="exampleFormControlSelect2" name="kategori">
                              <option value="biasa">Biasa</option>
                              <option value="promo">Promo</option>
                              <option value="baru">Baru</option>
                              <option value="pb">Promo dan Baru</option>
                            </select>
                          </div>
                          <div class="form-group">
                            <label for="exampleInputUsername1">Detail Product</label>
                            <input type="text" class="form-control" value="<?php echo $key->detail ?>" id="exampleInputUsername1" placeholder="Username" name="details">
                          </div>
                        </div>
                        <div class="modal-footer">
                          <button name="selesai" type="submit" class="btn btn-info btn-lg">Update</button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </td>
            </tr>
          <?php endforeach ?>

        </tbody>
      </table>
      <br><br>
    </div>
  </div>
</div>
</div>
<?php $this->load->view('admin/side/js') ?>