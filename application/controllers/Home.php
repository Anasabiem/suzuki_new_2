<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function index()
	{
		$data['produk'] = $this->M_suzuki->selectwhereliimit('product',array('kategori'=>'baru'))->result();
		$data['harga']=$this->M_suzuki->getharga()->result();
		$data['slider']=$this->M_suzuki->selectwhere('galeri',array('tipe'=>'slider'));
		$data['terbaru']=$this->M_suzuki->selectwhere('product',array('kategori'=>'baru'));
		$this->load->view('user/v_home',$data);
	}
}
