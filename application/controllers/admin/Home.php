<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function index()
	{
		if(!$this->session->userdata('status') == 'login'){
			redirect('LoginAdmin');
		}else{
			$data['galeri']=$this->M_suzuki->selectwhere('galeri',array('tipe'=>'slider'));
			$this->load->view('admin/v_home',$data);
		}
	}
	public function editSlider(){
		$config['upload_path']          = 'gallery/gambar_slider';
		$config['allowed_types']        = 'jpg|png|gif';
		$config['max_size']             = 0;
		$config['max_width']            = 0;
		$config['max_height']           = 0;
								// $date = date("His_dmY_");
		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload('userfile'))
		{
			$error = array('error' => $this->upload->display_errors());
			$this->load->view('admin/editSlider', $error);
		}
		else
		{
			$data = $this->upload->data();
			$name_file=$data['file_name'];
			$update_data['gambar_slider'] = $name_file;
			$id_slider=$this->input->post('id_slider');
			$update_data['judul_slider'] = $this->input->post('name');
			$update_data['isi_slider'] = $this->input->post('description');
			$this->suzuki_model->update('slider', $update_data, array('id_slider'=>$id_slider));
                        // $this->load->view('admin/Slider');
			header('location:'.base_url().'admin/Slider');
		}


		$id = $this->input->post('id');
		$galeri = $this->input->post('gambar_slider');
		$data = array('galeri'=>$galeri);
		$this->suzuki_model->update('galeri', $data, array('id_harga' => $id));
		header('location:'.base_url().'admin/Pricelist');
	}
}
