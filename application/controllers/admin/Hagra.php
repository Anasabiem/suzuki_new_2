<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hagra extends CI_Controller {

	public function index()
	{
		if(!$this->session->userdata('status') == 'login'){
			redirect('LoginAdmin');
		}else{
			$data['p']=$this->M_suzuki->select('product')->result();
			$data['harga']=$this->M_suzuki->getharga()->result();
			$this->load->view('admin/v_harga',$data);
		}
	}
	public function tambah(){
		$nm = $this->input->post('nmProduct');
		$tipe = $this->input->post('tipe');
		$harga = $this->input->post('harga');
		$data = array('idProduct' =>$nm ,
						'harga'=>$harga,
						'tipe'=>$tipe );
		$this->M_suzuki->insert('harga',$data);
		redirect(base_url('admin/Hagra'));
	}
	public function hapus($id){
		$where = array('idHarga'=>$id);
		$this -> M_suzuki -> delete($where,'harga');
		header('location:'.base_url('admin/Hagra'));
	}
}
