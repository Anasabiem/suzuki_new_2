<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LoginAdmin extends CI_Controller {

	public function index()
	{
		$this->load->view('user/v_login');
	}
	public function aksi_login(){
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$paasmd = md5($this->input->post('password'));
		$where = array('username' =>$username ,
						'password'=>$password );
		$cek = $this->M_suzuki->cek_login('admin',$where)->num_rows();
		if ($cek > 0) {
			$datasession = array('username' =>$username ,
								 'status'=>"login" );
			$this->session->set_userdata($datasession);
			redirect(base_url("admin/Product"));
		}else{
			redirect(base_url("LoginAdmin"));
		}

	}

	function logout(){
		$this->session->sess_destroy();
		redirect(base_url('Home'));
	}
}
